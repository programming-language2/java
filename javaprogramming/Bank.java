public class Bank
{
	private int minimumBalance = 2500;   //final can not be given once because the value changes. 
								//if given, the value cannot be changed further by this class itself
	
	public int getMinimumBalance()  //getter method  **method name ==> POJO class in Eclipse**
	{
		return this.minimumBalance;
	}
	
	public void setMinimumBalance(int value)  //args should be given for set method
	{	
		if (value>2500)
		this.minimumBalance = value;
	}
}