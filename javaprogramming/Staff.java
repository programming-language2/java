    public class Staff {  
    //instance variables of the class  
    int id;  
    String name;  
      
    Staff(){  
    System.out.println("this is no arg constructor");  
    }  
      
    Staff(int i, String n){  
    id = i;  
    name = n;  
		    System.out.println("this is parameterized constructor");  
    }  
      
    public static void main(String[] args) {  
    //object creation  
    Staff s = new Staff();  //if no constructor is created, then it calls default constructor
    System.out.println("\nDefault Constructor values: \n");  
    System.out.println("Staff Id : "+s.id + "\nStaff Name : "+s.name);  
      
    System.out.println("\nParameterized Constructor values: \n");  
    Staff dhoni = new Staff(7, "Dhoni");  
    System.out.println("Staff Id : "+dhoni.id + "\nStaff Name : "+dhoni.name);  
    } 
    }  