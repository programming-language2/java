public class Array{
public static void main(String [] args){
Array ar = new Array();
	//ar.sum();
	//ar.even();
	//ar.even1();
	//ar.odd();[[[
	//ar.total();
	//ar.average();
	//ar.grade();
	//ar.check();
	//ar.exam();
	//ar.day();
	//ar.serial();
	//ar.column();
	//ar.forEach();
	//ar.alternate();
	//ar.count();
	//ar.grid();
	//ar.leap();
	ar.ternary();
}
public void sum(){
	int[] sum = {98,87,76,65,34};
	int start=sum.length-1;
	while(start>=0){
	System.out.println(sum[start]);
		//start=start-1;
		start--;}

}
	
	public void even(){  //index even number
		int[] sum = {1,2,3,4,5,6};
		int start = 1;
		while(start<=sum.length){
		System.out.println(sum[start]);
			start=start+2;
		}
	}

	public void even1(){
			int[] sum = {22,21,34,42,51,64};
		int start = 0;
		while(start<sum.length){if(sum[start]%2==0) System.out.println(sum[start]);
		 start++;
		}
	}
	
	public void odd(){
			int[] sum = {22,21,34,42,51,64};
		int start = 0;
		while(start<sum.length){if(sum[start]%2!=0) System.out.println(sum[start]);
		 start++;
		}
	}
	
	public void total(){
		int[] sum = {1,2,3,4,5,6};
		//System.out.print(sum[0]+sum[1]+sum[2]+sum[3]+sum[4]+sum[5]);
		
		int total=0;
		for(int start=0;start<sum.length;start++){
			total=total+sum[start];
			
		}System.out.println(total);
	}
	
	public void average(){
		int[] sum = {92,83,64,75,35,25};
		//System.out.print(sum[0]+sum[1]+sum[2]+sum[3]+sum[4]+sum[5]);
		int n=sum.length;
		int total=0;
		for(int start=0;start<sum.length;start++){
			total=total+sum[start];
			
		}System.out.println(total/n);//System.out.println(total/sum.length);
	}
	
	public void grade(){
		int[] sum = {92,83,64,75,35,25};
		int avg = 92;
		if(avg>90 && avg<=100){System.out.println("1st g");}
			else if(avg>80 && avg<=90){System.out.println("2nd g");}
				else if(avg>70 && avg<=80){System.out.println("3rd g");}
					else if(avg>60 && avg<=70){System.out.println("4th g");}
						else if(avg>50 && avg<=60){System.out.println("5th g");}
							else {System.out.println("6th g");}
		
	}
	
	public void check(){
		int num = -79;
		if (num%2==0){ System.out.print("Even");}
		else {System.out.print("Odd");}
	}
	
	public void exam(){
		int mark = 49;
		if(mark>=50&&mark<=100){System.out.print("pass");}
		else if (mark>100){System.out.print("out of boundary");}
		else {System.out.print("fail");}
	}
	
	public void day(){
		int num = 7;
		if (num==1){System.out.print("sunday");}
		else if (num==2){System.out.print("monday");}
		else if (num==3){System.out.print("tuesday");}
		else if (num==4){System.out.print("wednesday");}
		else if (num==5){System.out.print("thursday");}
		else if (num==6){System.out.print("friday");}
		else if (num==7){System.out.print("saturday");}
	}
	
	public void serial(){
		int [] num = {1,1,1,1,1};
		int sum = 0;
		for(int i = 0; i<num.length; i++)
		{
		sum = sum+1;
		System.out.print(sum);}
	}
	
	public void column(){
		int [] num = {1,2,3,4,5};
		for(int r=0; r<num.length; r++){
		for(int i =0; i<num.length; i++)
		{System.out.print(num[r]);}
		System.out.println();}
		
	}
	
	public void forEach(){
		int [] num = {1,2,3,4,5};
		for(int mrk=0 ; mrk<=4; mrk++)
		{
		for(int src=0 ; src<=4; src++)
			{
				System.out.print(num[mrk]+" ");
			}
			System.out.println(" ");
		}
		
	}
	
	public void alternate(){
		int m[] = {10,20,30,40,50};
		int i=1;
		while(i<m.length)
		{
			System.out.println(m[i]);
			i=i+2;
		}
	}
	
	public void count(){
		int m [] = {98,34,56,78,27};
		int count = 0;
		int i = 0;
		while(i<m.length)
		{if (m[i]>50){count++;}
		 i++;}
		System.out.print(count);
	}
	
	public void grid(){
		for(int row = 1; row<=5; row++)
		{for(int col = 1; col<=5; col++)
		{System.out.print(row);}
		 System.out.println();
	}
	}
	public void leap(){
		int year=2024;
		if(year%4==0){System.out.print("leap year="+ year);}
		else {System.out.print("Not a leap year="+ year);}
	}
	
	public void ternary(){
		int no=101;
		String result = no>=50 && no<=100? "pass":"fail";
		System.out.print(result);
	}
}