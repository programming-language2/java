public class Pillai extends Naina
{
	public static void main(String [] args)
	{
	Naina ph = new Pillai();  //Dynamic binding or late binding
	//public ph1 = new Pillai();  //static binding or early binding
	ph.work();
	//ph.play();
	ph.study();
	}
	
	public void play()
	{
		System.out.println("play");
	}
		public void study()   //method has defined ==> so it is not an abstract method
	{	
		System.out.println("study");
	}
}