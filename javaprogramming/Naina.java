public abstract class Naina       //one of methods is abstract so class is also be abstract
{
	public abstract void study();  //Method is not defined ==> abstract method
	public void work()					//Have to use 'abstract' keyword after access modifier
	{
	System.out.println("work hard");
	}
	public void motivate()
	{
	System.out.println("motivate");
	}

}