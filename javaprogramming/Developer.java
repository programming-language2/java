public class Developer implements ABC
{
public static void main (String [] args)
{
Developer dev = new Developer();
ABC man = new Developer();
dev.attendance();
dev.purchase();
//dev.packages=1000;
System.out.println(dev.packages);

man.attendance();
man.purchase();
man.leisure(); //interface can not call concrete class method
}

public void attendance()
{System.out.println("attendance");
}
public void purchase()
{System.out.println("purchase");
}
public void sell()
{
}
public void profit()
{
}
public void leisure()
{
}
}