public interface ABC
{
	int packages = 100; //In Interface variables by default they are final and static
	public void attendance(); //In Interface methods by default they are abstract
							
	public void purchase();

	public void sell();

	public void profit();
}