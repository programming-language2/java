public class AccountHolder
{

	public static void main (String [] args)
	{
	Bank bb = new Bank();
	//System.out.println(bb.minimumBalance);   //anyway it is private, we can not access
	
		//bb.minimumBalance = 100;  //asigning new value
		//System.out.println(bb.minimumBalance);
		
		int min = bb.getMinimumBalance();  //calling getter method   
		System.out.println(min);
		
		bb.setMinimumBalance(3000);      //giving the value more than 2500
		min = bb.getMinimumBalance();  //calling set method
			System.out.println(min);
	}

}