package learnFileIO;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class FileDemo {

	private static Writer pen;

	public static void main(String[] args) {
		
		File note = new File("/home/sownesh/eclipse-workspace/BriyaniShop1/MuttonBriyani" + "/Menu_items.txt");
		//File ff1 = new File("/home/sownesh/eclipse-workspace/BriyaniShop/ChickenBriyani");

		//System.out.println(ff.mkdir());
		//System.out.println(ff.mkdirs());
		/*
		 * System.out.println(ff.canExecute()); System.out.println(ff.canRead());
		 * System.out.println(ff.canWrite()); System.out.println(ff.delete());
		 */
		
		
		try {
			note.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			note.createNewFile();			
			FileWriter penn= new FileWriter(note);
			/*
			 * pen.append("\nChicken Briyani"); pen.append("\nMutton Briyani"); pen.
			 * append("\nPostgreSQL is currently the most advanced open-source database available in the world. It was started in 1986 to overcome the problems of the contemporary database system. It was created by a computer science professor named Michael Stonebraker at UCB and was originally called Postgres, as it was started as a follow-up project and a post-Ingres project."
			 * );
			 */
			//pen.close();
		} catch (IOException e) {
						e.printStackTrace();
		}
		
		
		
		try {
			FileWriter penn= new FileWriter(note);
			
			BufferedWriter bw = new BufferedWriter(penn);
			bw .append("vijay");
			bw.newLine();
			bw .append("ajith");
			bw.newLine();
			bw .append("vikram");
			bw.newLine();
			bw.flush();
			bw.close();
			 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

}
