package learnFileIO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

public class Fdemo {

	public static void main(String[] args) throws IOException {
		Fdemo fd = new Fdemo();
		//fd.method1();
		//fd.method2();
		//fd.method3();
		//fd.method4();
		//fd. method5();
		fd.stream();
		
		
	}

	



	private void stream() throws IOException {
		File input = new File ("/home/sownesh/Pictures/day.jpg");
		File output = new File ("/home/sownesh/Pictures/day1.jpg");
		FileInputStream reader = new FileInputStream(input);
		FileOutputStream writer = new FileOutputStream(output);
		
		int i = reader.read();
		
		while (i!=-1) {
			System.out.println(i);
			writer.write(i);
			i=reader.read();
			
		}
	}





	/*
	 * private void method4() { File fr = new
	 * File("/home/sownesh/Rasika Files/Downloads/Employment Application Form (HRA-F004)(1).docx"
	 * ); FileReader ff = new FileReader(fr); for(int i = 0; i<ff.length; i++) { if
	 * (ff[i].isFile()) System.out.println(ff[i].getName()); }
	 */



	private void method1() {
		//list method
		File note = new File("/home/sownesh/Rasika Files/Downloads");
		String [] file_folder = note.list();
		for(int i = 0; i<file_folder.length; i++) {
			
			if (file_folder[i].endsWith(".mkv"))
				System.out.println(file_folder[i]);
		}
	}
	
	private void method2() {
		File note = new File("/home/sownesh/Rasika Files/Downloads");
		File[] ff = note.listFiles();
		for(int i = 0; i<ff.length; i++) {
			if (ff[i].isDirectory())
				System.out.println(ff[i].getName());
		}
	}
	

	private void method3() {
		File note = new File("/home/sownesh/Rasika Files/Downloads");
		File[] ff = note.listFiles();
		for(int i = 0; i<ff.length; i++) {
			if (ff[i].isFile())
				System.out.println(ff[i].getName());
		}	
	}


	private void method5() {
				File note = new File ("/home/sownesh/Rasika Files/Downloads/file.txt");
				
				try {
					FileReader reader = new FileReader (note);
					BufferedReader br = new BufferedReader(reader);
					String line = br.readLine();
					while (line!=null) {
						System.out.println(line);
						line = br.readLine();
						
						
					}
					
					int i = reader.read();
					while(i!=1) {
						System.out.println((char)i);
						i=reader.read();
					}
				} catch (FileNotFoundException e) {
										e.printStackTrace();
				} catch (IOException io) {
					io.printStackTrace();
				}
	}


}
