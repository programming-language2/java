package scanner;

import java.util.Scanner;

public class ScannerDemo {

	public static void main(String[] args) {
		/*
		 * Scanner sc = new Scanner(System.in); System.out.println("Enter your name");
		 * String name = sc.nextLine(); System.out.println("hello " + name);
		 * 
		 * System.out.println("Enter your age: "); int age = sc.nextInt();
		 * 
		 * System.out.println("Enter your height: "); double height = sc.nextDouble();
		 * 
		 * System.out.println("Enter your mob: "); long mob = sc.nextLong();
		 * 
		 * System.out.println("Age :"+age); System.out.println("Height :"+height);
		 * System.out.println("Mobile :"+mob);
		 */
		
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter your no : ");
		int no = sc.nextInt();
		String [] name = new String [no];
		System.out.println("your array length is : " + name.length);
		
		for(int i=0; i<name.length; i++) {
			System.out.println("Enter your name : ");
			name[i]= sc.next();
		}
		
		for(int i=0; i<name.length; i++) {
			System.out.println(name[i]);
		}
	}

}
