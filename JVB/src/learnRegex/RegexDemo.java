package learnRegex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexDemo {
	
	public static void main(String[] args) {
		RegexDemo rd = new RegexDemo();
		String [] arg = {"yogesh", "aravind"};
		
		rd.main(arg,0);
	}

	public void main(String[] args, int no) {
		
		/*
		 * System.out.println(args[0]); System.out.println(args[1]);
		 */
		
		
		String input = "My mobile number is 9884010000";

		Pattern patternObj = Pattern.compile("\\d{10}");
		Matcher matcherObj = patternObj.matcher(input);
		while (matcherObj.find()) {
			System.out.println(matcherObj.group());
			System.out.println(matcherObj.start());
			System.out.println(matcherObj.end());

		}
		 
	}

}
