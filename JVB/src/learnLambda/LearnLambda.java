package learnLambda;

import java.util.function.Function;
import java.util.function.Predicate;

public class LearnLambda {

	public static void main(String[] args) {
		/*//FunctionalInterfaceDemo fid = ()->System.out.println(500+20);
		FunctionalInterfaceDemo fid = (no1, no2)->{return no1+no2;};
		System.out.println(	fid.add2(10,20));
		//fid.add();
		FunctionalInterfaceDemo.play();
		fid.run();
		fid.run2();
		*/
		
		Function<Integer, Integer> f=i->{return i;};
		System.out.println(f.apply(16));
		
		Predicate<Integer> h = i->{if (i<=10)
			return true;
		return false;
		};
		System.out.println(h.test(15));
	}
	/*
	 * public void add() { System.out.println(); }
	 */
	
	
	/* int  */
	
}
	
