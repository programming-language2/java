package operator;

public class OperatorDemo {

	public static void main(String[] args) {
		//System.out.println(5/10);
		
		int no=10;
		//post unary operator
		System.out.println(no++);
		System.out.println(no);
		
		System.out.println(no--);
		System.out.println(no);
		
		//pre unary operator
		System.out.println(++no);
		System.out.println(no);
		
		System.out.println(--no);
		
		//and git
		int no1 =10;
		int no2 = 20;
		//System.out.println(no1<no2 && ++no1);
		//System.out.println(no1<no2 && --no1);
		System.out.println(no2);
			
	}

}
