package learnString;

public class StringMethods {

	public static void main(String[] args) {
		
		
		String name1 = "santhosh";
				String name2 = "santhosh";
				System.out.println(name1.length());
				
				for(int i = 0; i<name1.length(); i++) {
				System.out.println(name1.charAt(i));}
				
				System.out.println(name1.startsWith("San"));
				System.out.println(name2.endsWith("osh"));
				
				String date = "10/08/2023";
				String [] ar = date.split("/");
				
				for (String str : ar)
				{ System.out.println(str);
				
				System.out.println(name1.compareTo(name2));
				System.out.println(name1.compareToIgnoreCase(name2));
				System.out.println(name1.concat(" K"));
				
				//substring method
				System.out.println(name1.substring(4));
				System.out.println(name1.substring(2, 4));
				System.out.println(name2.contains("th"));
				System.out.println(name1.equals(name2));
				System.out.println(name1.equalsIgnoreCase("santhosh"));
				
				String name3 = "";
				System.out.println(name3.isEmpty());
				
				System.out.println(name1.indexOf('s'));
				System.out.println(name1.lastIndexOf('s'));
				
				char [] ch = name1.toCharArray();
				System.out.println(name1.toUpperCase());
				System.out.println(name1.toLowerCase());
				
				String name = "  rasi ka  ";
				System.out.println(name.length());
				name = name.trim();
				System.out.println(name.length());
				System.out.println(name);
			
				
	}

	}
}
