package learnString;

public class StringLearnings {

	int price;
	public static void main(String[] args) {
		String ss = new String("apple");
		System.out.println(ss);
		
		StringLearnings sl = new StringLearnings();
		sl.price=200;
		System.out.println(sl);
		//learnString.StringLearnings@d041cf  ----> hashcode
		// hashcode --->  hexadecimal 0 9 a b c d e f
		
		System.out.println(sl.hashCode());
		

		StringLearnings sl2 = new StringLearnings();
		sl2.price=300;
		System.out.println(sl2.hashCode());
		
		//variable comparison
		int no1 = 5, no2 = 6;
		if(no1==no2) {System.out.println("True");}
		else {System.out.println("False");}
		
		//object comparison should not do like this
		if (sl==sl2) {System.out.println("Both objects are equal");}
		else {System.out.println("Both objects are not equal");}
		
		//can compare two properties of the method by using equals method
		System.out.println(sl.equals(sl2));
		if (sl.equals(sl2)) {System.out.println("Both objects are equal");}
		else {System.out.println("Both objects are not equal");}
		
		
	}
	
	@Override
	public String toString() {
				return "Mango";
	}

	@Override
	public boolean equals(Object obj) {
		StringLearnings sls = (StringLearnings)obj;
		if(this.price==sls.price)
		return true;
		else
			return false;
	}
	
	@Override
	public int hashCode() {
				return 123;
	}
}
