package learnThread;

public class ThreadDemo {

	public static void main(String[] args) {
		ThreadChild  td = new ThreadChild ();
		//td.run();
		ThreadChild tc = new ThreadChild ();
		
		td.start();
		System.out.println(td.getPriority());
		System.out.println(td.getName());
		System.out.println(td.getId());
		System.out.println(td.isDaemon());
		//td.start();  IllegalThreadStateException
		tc.start();
		System.out.println(tc.getPriority());
		System.out.println(tc.getName());
		System.out.println(tc.getId());
		System.out.println(tc.isDaemon());
		
		for (int i = 1; i<=5; i++) {
			System.out.println("ThreadDemo"+i);
		}
	}

}
