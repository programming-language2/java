package learnException;

import java.io.IOException;

public class Marks {

	public static void main(String[] args) throws RuntimeException, IOException {
		int [] marks = {89,90,91,92,93};
		calculate(marks);
	}

	public static void calculate(int [] marks) throws ArrayIndexOutOfBoundsException, IOException{
		for(int i=0; i<5; i++) {
			System.out.println(marks[i]);
		}
	}

}
