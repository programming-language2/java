package learnException;

public class ErrorDemo {

	public static void main(String[] args) {
		ErrorDemo ed = new ErrorDemo();
		ed.display();
	}

	private void display() {
		System.out.println("hi");	
		display();
		
//		try {}       can't use in Error
//		catch(Throwable e) {}
	}

}
