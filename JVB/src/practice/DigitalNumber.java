package practice;

public class DigitalNumber {

	public static void main(String[] args) {
		DigitalNumber dn = new DigitalNumber();
		
		dn.zero();
		dn.space();
		dn.one();
		dn.space();
		dn.two();
		dn.space();
		dn.three();
		dn.space();
		dn.four();
		dn.space();
		dn.five();
		dn.space();
		dn.six();
		dn.space();
		dn.seven();
		dn.space();
		dn.eight();
		dn.space();
		dn.nine();
		
	}

	private void nine() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( row==1 || row==7 || row==4 || col==1 && row<=4 || col==7 )
				System.out.print("◙ ");
				else System.out.print("  ");
			}
			System.out.println();
		}			
	}

	private void eight() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( row==1 || row==7 || row==4 || col==1 || col==7 )
				System.out.print("◙ ");
				else System.out.print("  ");
			}
			System.out.println();
		}		
	}

	private void seven() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( row==1||col==7)
				System.out.print("◙ ");
				else System.out.print("  ");
			}
			System.out.println();
		}			
	}

	private void six() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( row==1 || row==7 || row==4 || col==1 || col==7 && row>3)
				System.out.print("◙ ");
				else System.out.print("  ");
			}
			System.out.println();
		}		
	}

	private void five() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( row==1 || row==7 || row==4 || col==1 && row<4 || col==7 && row>3)
				System.out.print("◙ ");
				else System.out.print("  ");
			}
			System.out.println();
		}			
	}

	private void four() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( col==1 && row<=4 ||col==7||row==4)
				System.out.print("◙ ");
				else System.out.print("  ");
			}
			System.out.println();
		}
	}

	private void three() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( row==1 || row==7 || row==4 && (col!=1 && col!=2)|| col==7 )
				System.out.print("◙ ");
				else System.out.print("  ");
			}
			System.out.println();
		}	
	}

	private void two() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( row==1 || row==7 || row==4 || col==1 && row>3 || col==7 && row<4)
				System.out.print("◙ ");
				else System.out.print("  ");
			}
			System.out.println();
		}	
	}

	private void one() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( col==4)
				System.out.print("◙ ");
				else System.out.print("  ");
			}
			System.out.println();
		}			
	}

	private void zero() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( row==1||col==1||col==7||row==7)
				System.out.print("◙ ");
				else System.out.print("  ");
			}
			System.out.println();
		}	
		
	}
	private void space() {
		for (int row = 1; row <= 7; row++) {
			System.out.print(" ");
		}	System.out.println();	
	}

}
