package practice;

public class Patterns {

	public static void main(String[] args) {
		Patterns p = new Patterns();
		// p.skeleton();
		// p.patter2();
		// p.patterI();
		//p.patternC();
		//p.patternO();
		//p.patternD();
		//p.patternl();
		//p.patternn();
		//p.patternD();
		//p.patternB();
		//p.patternR();
		//p.patternN();
		//p.patternX();
		//p.patternY();
		//p.patternY2();
		//p.patternZ();
		//p.patternH();
	    //p.patternA();
		//p.patternA2();
		//p.patternM();
		p.patternS();
		//p.pattern();
	}

	private void pattern() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( row==1||row==7||col==1||col==7) {
				 if ((row==1&&col==1) || (row==1&&col==7) || (row==7&&col==1) || (row==7&&col==7))
					System.out.print("  ");}
				else System.out.print("* ");
			}
			System.out.println();
		}		
	}

	private void patternS() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if ((row==1 && col!=1) || (row==4 && (col!=1 && col!=7)) || (row==7 && col!=7))
				System.out.print("* ");
				else if((row==2&&col==1)||(row==3&&col==1))
				System.out.print("* ");
				else if((row==5&&col==7)||(row==6&&col==7))
				System.out.print("* ");
				else System.out.print("  ");
			}
			System.out.println();
		}			
	}

	private void patternA2() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( row==4||col==1||col==7||row==1) {
					if((row==1&&col==1)|| (row==1&&col==7))
						System.out.println(" ");
					else
				System.out.print("* ");}
				else System.out.print("  ");
			}
			System.out.println();
		}			
	}

	private void patternM() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( (col==row && col<=4)|| (col+row==8 && row<=4)|| col==1 || col==7)
				System.out.print("* ");
				else System.out.print("  ");
			}
			System.out.println();
		}	
	}

	private void patternA() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( row==4||(col==1 && row>3)||(col==7 && row>3)|| row+col==5 || col-row==3)
				System.out.print("* ");
				else System.out.print("  ");
			}
			System.out.println();
		}			
	}

	private void patternH() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( row==4||col==1||col==7)
				System.out.print("* ");
				else System.out.print("  ");
			}
			System.out.println();
		}	
	}

	private void patternY2() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( (col==row && col<=4)|| (col+row==8 && row<=4)|| (col==4 && row>4))
				System.out.print("* ");
				else System.out.print("  ");
			}
			System.out.println();
		}	
	}

	private void patternZ() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( row==1||row==7||col+row==8)
				System.out.print("* ");
				else System.out.print("  ");
			}
			System.out.println();
		}	
	}

	private void patternY() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( (col==row && col<=4)|| col+row==8)
				System.out.print("* ");
				else System.out.print("  ");
			}
			System.out.println();
		}			
	}

	private void patternX() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if( col==row || col+row==8)
				System.out.print("* ");
				else System.out.print("  ");
			}
			System.out.println();
		}			
	}

	private void patternN() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if(col==1 || (col==row)|| col==7 )
				System.out.print("* ");
				else System.out.print("  ");
			}
			System.out.println();
		}		
	}

	private void patternR() {
		patternD();
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if(col==1 || (col==row))
				System.out.print("* ");
				else System.out.print("  ");
			}
			System.out.println();
		}
	}

	private void patternn() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == 1 || row == 7 || col == 1 || col==7) {
					if (row == 1 && col == 7)
						System.out.print("  ");
					else if (row == 7)
						System.out.print("");
					else
						System.out.print("* ");
				} else
					System.out.print("  ");
			}
			System.out.println();
		}	
	}

	private void patternB() {
		patternD();
		
		
	}

	private void patternl() {
				for(int i=1; i<=7; i++) {
					System.out.println("*");
				}
	}

	private void patternD() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == 1 || row == 7 || col == 1 || col==7) {
					if (row == 1 && col == 7)
						System.out.print("  ");
					else if (row == 7 && col == 7)
						System.out.print("  ");
					else
						System.out.print("* ");
				} else
					System.out.print("  ");
			}
			System.out.println();
		}		
	}

	private void patternO() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == 1 || row == 7 || col == 1 || col==7) {
					if (row == 1 && (col == 1 || col==7))
						System.out.print("  ");
					else if (row == 7 && (col == 1 || col==7))
						System.out.print("  ");
					else
						System.out.print("* ");
				} else
					System.out.print("  ");
			}
			System.out.println();
		}		
	}

	private void patternC() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == 1 || row == 7 || col == 1) {
					if (row == 1 && col == 1)
						System.out.print("  ");
					else if (row == 7 && col == 1)
						System.out.print("  ");
					else
						System.out.print("* ");
				} else
					System.out.print("  ");
			}
			System.out.println();
		}
	}

	private void patterI() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == 1 || row == 7 || col == 4)
					System.out.print("* ");
				else
					System.out.print("  ");
			}
			System.out.println();
		}
	}

	private void patter2() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				if (row == 1 || row == 7)
					System.out.print("* ");
				else
					System.out.print("# ");
			}
			System.out.println();
		}
	}

	private void skeleton() {
		for (int row = 1; row <= 7; row++) {
			for (int col = 1; col <= 7; col++) {
				System.out.print("# ");
			}
			System.out.println();
		}
	}

}
