package learnTypeCasting;

public class Engineer extends Student{

	
	public static void main(String[] args) {
		Engineer child = new Engineer();
		Engineer child2 = new Engineer();
		String name = "name";
		child.doProjects();
		child.study();
		child.hashCode();
		System.out.println(child.hashCode());
		System.out.println(child2.hashCode());
		System.out.println(name.hashCode());
		System.out.println(child.equals(child2));
		
		
	
		  Student parent = (Student)child; //up casting/widen casting 
		  //parent.study();
		 
		
		Student ss = new Engineer();
		Engineer ee = (Engineer)ss;   //down casting/Narrow casting
		//ee.doProjects();
		//ee.study();
		
		
		
	}
	@Override
	public int hashCode() {
				return super.hashCode();   // return 123;
	}

	private void doProjects() {
		System.out.println("doProject");
	}
}
