package learnTypeCasting;

public class TypeCastingDemo {

	public static void main(String[] args) {
		float price = (float)102.5;
		double price2 = (double)price;
		char price3 = (char)price;
		byte price4 = (byte)price;
		//boolean price5 = boolean(price);
		
		int ch = 'A';
		System.out.println(ch);
	}

}
