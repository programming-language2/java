package learnCollections;

import java.util.ArrayList;

public class ArrayListTask {

	public static void main(String[] args) {
		 
		ArrayList al = new ArrayList();
		
		al.add(10);
		al.add(10);
		al.add(10);
		al.add(40);
		al.add(50);
		al.add("appu");
		al.add(true);
		al.add(5.2);
		al.add(5, "K");
		
		//System.out.println(al);
		//System.out.println(al.size());
		//System.out.println(al.contains(al));
		
		//to remove duplicate elements
		int []ar = {10, 10, 10, 40,40,60,50,70, 50};
		ArrayList al2 = new ArrayList<>();
		
		for (int i : ar) {
			if (!al2.contains(i)) {
				al2.add(i);
			}
		}System.out.println(al2);
		
	}

}
