package learnCollections;

import java.util.ArrayList;

public class ArrayListDemo {
	
	


	public static void main(String[] args) {
		
		int no = 10;
		Integer i = no;  //AutoBoxing
		int no1 = i;  //AutoUnBoxing
		
		
		
		ArrayList al = new ArrayList();
		al.add("santhosh");
		al.add(i);
		al.add(true);
		al.add(5.4f);
		al.add("santhosh");
		System.out.println(al);
		System.out.println(al.contains("rasika"));
		System.out.println(al.get(0));
		System.out.println(al.indexOf(true));
		System.out.println(al.lastIndexOf("santhosh"));
		System.out.println(al.remove(4));
		System.out.println(al);
		
		ArrayList al2 = new ArrayList();
		al2.addAll(al);
		al2.add("rasika");
		al2.add(22);
		
		
		System.out.println("al2"+al2);
		System.out.println(al2.containsAll(al2));
		//System.out.println(al2.retainAll(al));
		System.out.println(al2);
		System.out.println(al2.removeAll(al));
		System.out.println(al2);
		
	}

}
