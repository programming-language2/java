package learnCollections;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class LearnMap {

	public static void main(String[] args) {
		HashMap hm = new HashMap<>();
		
		hm.put("Pongal", 60);
		hm.put("idli", 40);
		hm.put("Dosai", 60);
		hm.put("poori", 50);
		
		hm.put("Pongal", 68);
		
		System.out.println(hm);
		System.out.println(hm.get("poori"));
		//System.out.println(hm.remove("Pongal"));
		System.out.println(hm.containsKey("Dosai"));
		System.out.println(hm.containsValue(50));
		
		//Key => pongal, poori, dosai => Keyset
		//Value => 60, 50, 60 => collection
		
		//Key     Value    K+V= Entry
		//Pongal	60		EntrySet
		//Poori		50		EntrySet
		//dosai		60		EntrySet
		
		System.out.println(hm.keySet());
		System.out.println(hm.values());
		System.out.println(hm.entrySet());
		
		
		Iterator i = hm.entrySet().iterator();
		
		
		while(i.hasNext()) {
			Map.Entry me = (Map.Entry)i.next();  //downcasting
			System.out.println(me.getKey());
			System.out.println(me.getValue());
			if(me.getKey().equals("Pongal"))
				me.setValue((int)me.getValue()+5);
				
		}System.out.println(hm);
	}

}
