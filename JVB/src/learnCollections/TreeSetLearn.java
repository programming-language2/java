package learnCollections;

import java.util.TreeSet;

public class TreeSetLearn {

	public static void main(String[] args) {
		
		TreeSet names = new TreeSet();
		names.add("A");
		names.add("B");
		names.add("C");
		names.add("D");
		
		System.out.println(names.isEmpty());
		System.out.println(names.getClass());
		System.out.println(names);
		
	}

}
